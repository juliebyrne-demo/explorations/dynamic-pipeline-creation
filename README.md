# Dynamic pipeline creation

Uses a ruby script to generate the same job for each subdirectory that uses rules: changes to only run when something in that subdirectory changes.

Useful for cases where you have a large repo and only want to validate/test portions of the code base that changed.

see `generate-configs` for script that generates a `service-config.yml` file that is used by the main `.gitlab-ci.yml` for the triggered child pipeline.

For this simple case, we assume that the `srcdir` directory contains a set of subdirectories in it, and no individual files are stored directly wtihin `srcdir`.

See the [documentation](https://docs.gitlab.com/ee/ci/yaml/#trigger-child-pipeline-with-generated-configuration-file) for more information on dynamically generated configuration files.

This example extends the example shown in the video [Create Child Pipelines Using Dynamically Generated Configurations](https://www.youtube.com/watch?v=nMdfus2JWHM&feature=youtu.be) 